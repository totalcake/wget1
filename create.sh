#!/bin/bash

GITLAB_API=https://gitlab.com/api/v4
NAME_SPACE=totalcake
PROJECT_TEMPLATE=typescript-template

# TODO add help

while [[ "$#" -gt 0 ]]; do
   case $1 in
      -n|--name) PROJECT_NAME="$2"; shift;;
      -k|--token) PRIVATE_TOKEN="$2"; shift;;
      -s|--name-space) NAME_SPACE="$2"; shift;;
      -t|--template ) PROJECT_TEMPLATE="$2"; shift;;
      *) echo "Unknown parameter: $1"; exit 1;;
   esac
   shift
done

# fork projet
wget --no-check-certificate \
  --method POST \
  --timeout=0 \
  --header 'Content-Type: application/json' \
  --body-data '{
    "path" : "'${PROJECT_PATH}'",
    "name": "'${PROJECT_NAME}'"}' \
   "${GITLAB_API}/projects/${NAME_SPACE}%2F${PROJECT_TEMPLATE}/fork?private_token=${PRIVATE_TOKEN}"

# TODO exit if not ok

# waiting import is finished
IMPORT_STATUS=""
while [[ ${IMPORT_STATUS} != "finished" ]];
do
    IMPORT_STATUS="`wget -qO - "${GITLAB_API}/projects/${NAME_SPACE}%2F${PROJECT_NAME}/import?private_token=${PRIVATE_TOKEN}" | grep -o '"import_status":"[^"]*' | grep -o '[^"]*$'`"
    echo $IMPORT_STATUS
done
# remove fork relationships
wget --no-check-certificate --quiet \
  --method DELETE \
  --timeout=0 \
   "${GITLAB_API}/projects/${NAME_SPACE}%2F${PROJECT_NAME}/fork?private_token=${PRIVATE_TOKEN}"
# clone project
git clone git@gitlab.com:${NAME_SPACE}/${PROJECT_NAME}.git
# cd project
cd ${PROJECT_NAME}
# install packages
npm install
